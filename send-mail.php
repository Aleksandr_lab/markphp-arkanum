<?php
$response = false;
$email_a = $_POST['email'];
if (!filter_var($email_a, FILTER_VALIDATE_EMAIL)) {
    $response = [
        'success' => false,
        'error'=> 'Email error'
    ];
    echo json_encode($response);
    die();
}

if (empty($_POST['recaptcha'])) {
    echo 'success-false';
    die();
} else {
    $secret   = '6Lf3I8sUAAAAALOxAJjYNl1HWCM1-8Tp8gaEPaU9';
    $response = file_get_contents(
        "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $_POST['recaptcha'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']
    );
    $response = json_decode($response);

    if ($response->success === false) {
         echo json_encode($response);;
        die();
    }
}

if ($response && $response->success==true && $response->score <= 0.5) {
    echo json_encode($response);;
    die();
} else {
    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );
    $from = $email_a;
    $to = "academws1@gmail.com";
    $subject = "PHP Mail Test script";
    ob_start();?>
    <h3 style="color: #343a40;">
        <b>Name:</b>
    </h3>
    <p style="font-size: 18px;">
        <?= $_POST['name'];?>
    </p>
    <h3 style="color: #343a40;">
        <b>Message:</b>
    </h3>
    <p style="font-size: 18px;">
        <?= $_POST['message'];?>
    </p>
    <?php
    $message = ob_get_clean();
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From:" . $from;
    mail($to,$subject,$message, $headers);
    echo json_encode($response);;
    die();
}
