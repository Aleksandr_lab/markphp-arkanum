# Webpack

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles files development build
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints js and fixes files
```
npm run lint:js
```
### PhpStorm eslint syntax highlighting
(Ctrl+Alt+S)
 	-- Languages and Frameworks | JavaScript | ESLint 
 and select the Automatic ESLint configuration option.
 
 ### Lints scss and fixes files
 ```
npm run lint:css
 ```
### PhpStorm stylelint syntax highlighting
(Ctrl+Alt+S)
 	-- Languages and Frameworks | Style Sheets | Stylelint 
 and select Enable.
 
*Macros*
```
Tools | External tools 
add
Program: npm 
Arguments:  run lint:css
Working directory: $FileDir$

************************************

Keymap | External Tools [name_tools] and select keyboard shortcuts 
```
### [Repository build](https://gitlab.com/Aleksandr_lab/markphp-arkanum)


