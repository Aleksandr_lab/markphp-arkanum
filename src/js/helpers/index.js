const _t = (str) => {
  if (typeof str === 'string') {
    return str.toLowerCase().replace(/[^a-z0-9]/g, '');
  }
  throw new Error(`Methods '_t' accepts only string. Got ${typeof str} instead.`);
};

export { _t };
