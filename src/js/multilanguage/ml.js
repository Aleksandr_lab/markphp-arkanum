import Vue from 'vue';
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage';
import eng from './langs/eng';
import de from './langs/de';

Vue.use(MLInstaller);

export default new MLCreate({
  initial: 'ENG',
  save: process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development',
  languages: [
    new MLanguage('ENG').create(eng),
    new MLanguage('DE').create(de),
  ],
});
