import Vue from 'vue';
import VueSimpleSVG from 'vue-simple-svg';
import App from './App.vue';
import router from './routes';
import './multilanguage/ml';
import { _t } from './helpers';

Vue.prototype.$_t = _t;
Vue.use(VueSimpleSVG);

Vue.directive('click-outside', {
  bind(el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      if (!(el === event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent);
  },
  unbind(el) {
    document.body.removeEventListener('click', el.clickOutsideEvent);
  },
});

const main = new Vue({
  el: '#app',
  router,
  render: (h) => h(App),
});
