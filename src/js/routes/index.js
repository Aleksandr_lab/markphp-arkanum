import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routesData/allRoutes';
import deLang from '../multilanguage/langs/de';
import engLang from '../multilanguage/langs/eng';
import { _t } from '../helpers';

Vue.use(VueRouter);


/* Add Vue Router */

const router = new VueRouter({
  mode: 'history',
  routes,
});


router.beforeEach((to, from, next) => {

  const nearestWithTitle = to.matched.slice().reverse().find((r) => r.meta && r.meta.title);
  const nearestWithMeta = to.matched.slice().reverse().find((r) => r.meta && r.meta.metaTags);

  if (nearestWithTitle) {
    switch (localStorage.getItem('vueml-lang')) {
      case 'DE':
        document.title = deLang[_t(nearestWithTitle.meta.title)];
        break;
      default:
        document.title = engLang[_t(nearestWithTitle.meta.title)];
        break;
    }
  }

  document.querySelectorAll('[data-vue-router-controlled]').forEach((el) => el.parentNode.removeChild(el));
  if (!nearestWithMeta) return next();

  nearestWithMeta.meta.metaTags.map((tagDef) => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach((key) => {
      tag.setAttribute(key, tagDef[key]);
    });
    tag.setAttribute('data-vue-router-controlled', '');
    return tag;
  }).forEach((tag) => document.head.appendChild(tag));
  next();
});

export default router;
