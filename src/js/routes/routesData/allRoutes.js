/* Component page */
const Home = () => import(/* webpackChunkName: "home-page" */ '../../components/page/home/Home.vue');
const AboutUs = () => import(/* webpackChunkName: "about-us-page" */ '../../components/page/about/AboutUs.vue');
const Faq = () => import(/* webpackChunkName: "faq-page" */ '../../components/page/faq/Faq.vue');
const Contact = () => import(/* webpackChunkName: "contacts-page" */ '../../components/page/contact/Contact.vue');
const NotFound = () => import(/* webpackChunkName: "not-found-page" */ '../../components/page/error/NotFound.vue');
const Legals = () => import(/* webpackChunkName: "legals-page" */ '../../components/page/legals/Legals.vue');
const Terms = () => import(/* webpackChunkName: "terms-page" */ '../../components/page/terms/TermsService.vue');
const Privacy = () => import(/* webpackChunkName: "privacy-page" */ '../../components/page/privacy/Privacy.vue');
const PrivacyDocs = () => import(/* webpackChunkName: "privacy-docs-page" */ '../../components/page/privacy-docs/PrivacyDocs.vue');
const Cookies = () => import(/* webpackChunkName: "cookies-page" */ '../../components/page/cookies/Cookies.vue');

export default [
  {
    path: '/',
    name: 'home',
    notIndex: true,
    meta: {
      title: 'Arkanum'
    },
    component: Home,
  }, {
    path: '/',
    name: 'accounts',
    meta: {
      title: 'accounts'
    },
  }, {
    path: '/',
    name: 'transfers',
    meta: {
      title: 'transfers'
    },
  }, {
    path: '/',
    name: 'payments-cards',
    meta: {
      title: 'payments-cards'
    },
  }, {
    path: '/about-us',
    name: 'about-us',
    meta: {
      title: 'about-us'
    },
    component: AboutUs,
  }, {
    path: '/faq',
    name: 'faq',
    meta: {
      title: 'faq'
    },
    component: Faq,
  }, {
    path: '',
    name: 'legals',
    meta: {
      title: 'legals'
    },
    component: Legals,
    children: [
      {
        path: '/privacy-docs',
        meta: {
          title: 'Privacy Docs'
        },
        name: 'privacy-docs',
        component: PrivacyDocs,
      }, {
        path: '/terms',
        meta: {
          title: 'Terms of service'
        },
        name: 'terms-of-service',
        component: Terms,
      }, {
        path: '/aml-docs',
        meta: {
          title: 'aml-docs'
        },
        name: 'aml-docs',
        component: Privacy,
      }
    ],
  }, {
    path: '/contacts',
    name: 'contacts',
    meta: {
      title: 'Contacts'
    },
    component: Contact,
  }, {
    path: '/cookies',
    notIndex: true,
    meta: {
      title: 'Cookies'
    },
    name: 'cookies',
    component: Cookies,
  }, {
    path: '*',
    notIndex: true,
    name: 'not-found',
    meta: {
      title: 'Page Not Found',
      metaTags: [
        {
          name: 'robots',
          content: 'noindex',
        }
      ]
    },
    component: NotFound,
  }
];
